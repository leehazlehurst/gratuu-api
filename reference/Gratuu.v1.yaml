openapi: 3.0.0
info:
  title: Gratuu
  version: '1.0'
  contact:
    name: Lee Hazlehurst
  description: |
    Gratuu API documentation
servers:
  - url: 'https://api.gratuu.com'
paths:
  /account:
    get:
      summary: Current logged in account info
      tags:
        - Account
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  bankDetailsValid:
                    type: boolean
                  accountBalance:
                    type: number
                  historicalTotal:
                    type: number
                  firstName:
                    type: string
                  gratuuId:
                    type: string
                  lastName:
                    type: string
                  taxInfoValid:
                    type: string
                  pAYEInfoValid:
                    type: string
                    deprecated: true
                  isReceiver:
                    type: boolean
              examples: {}
            application/xml:
              schema:
                $ref: '#/components/schemas/AccountOverview'
      operationId: get-Account
      description: Basic Account info for the currently logged in user
      security:
        - OIDC: []
    parameters: []
    post:
      summary: ''
      operationId: post-account
      responses:
        '200':
          description: OK
      description: Set account level info
      requestBody:
        content:
          application/json:
            schema:
              type: object
              properties:
                firstName:
                  type: string
                lastName:
                  type: string
                isReceiver:
                  type: boolean
      security:
        - OIDC: []
      tags:
        - Account
  '/{id}/qr':
    parameters:
      - schema:
          type: string
        name: id
        in: path
        required: true
        description: Gratuu ID of the user
    get:
      summary: Account QR code image
      tags: []
      responses:
        '200':
          description: OK
          content:
            image/png:
              schema: {}
      operationId: get-id-qr
      description: Returns a png for a QR code linking directly to the specified users profile
  '/tip/{tipId}/payment':
    parameters:
      - schema:
          type: string
        name: tipId
        in: path
        required: true
    post:
      summary: Create Payment Intent
      operationId: post-tip-tipId-payment
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/PaymentInfo'
      description: Create a payment intent for a tip
      tags:
        - Payment
  /tips:
    get:
      summary: User Tips
      tags:
        - tips
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/TipInfo'
        '403':
          description: Forbidden
      operationId: get-tips
      parameters:
        - schema:
            type: string
          in: query
          name: userId
          description: '[ADMIN ONLY] Optional ID of the user to query'
      description: Get all tips for a user
      security:
        - OIDC: []
    post:
      summary: ''
      operationId: post-tips
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/TipInfo'
      description: Create a new tip record
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CreateTipModel'
      tags:
        - tips
  '/tips/{tipId}':
    parameters:
      - schema:
          type: string
        name: tipId
        in: path
        required: true
    get:
      summary: Get Tip Info
      tags:
        - tips
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/TipInfo'
      operationId: get-tips-tipId
      description: Get a specific tip info
      security:
        - OIDC: []
  /paye:
    get:
      summary: User PAYE Info
      tags:
        - paye
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/PAYEInfo'
      operationId: get-paye
      description: Returns account info that is used for PAYE registration for the currently logged in user.
      security:
        - OIDC: []
    post:
      summary: PAYE Info
      operationId: post-paye
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/PAYEInfo'
      description: Update PAYE info for the current user
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/PAYEInfo'
      tags:
        - paye
      security:
        - OIDC: []
  '/profile/{id}':
    parameters:
      - schema:
          type: string
        name: id
        in: path
        required: true
    get:
      summary: Profile Information
      tags:
        - profile
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ProfileDetails'
      operationId: get-profile-gratuuId
      description: Profile Information
    post:
      summary: Update Profile
      operationId: post-profile-id
      responses:
        '200':
          description: OK
        '403':
          description: Forbidden
        '404':
          description: Not Found
      security:
        - OIDC: []
      requestBody:
        content:
          application/json:
            schema:
              type: object
              properties:
                companyName:
                  type: string
                jobTitle:
                  type: string
      description: Update Profile info
      tags:
        - profile
  /account/bankInfo:
    get:
      summary: Get Bank Info
      tags:
        - Account
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  field1:
                    type: string
                    description: for UK this is account Number
                  field2:
                    type: string
                    description: for UK this is sort-code
      operationId: get-account-bankInfo
      security:
        - OIDC: []
      description: |-
        Get current bank info.
        field1 is:
          UK - Account Number

        field2 is:
          UK - sort-code
    post:
      summary: Update Bank Info
      operationId: post-account-bankInfo
      responses:
        '200':
          description: OK
      requestBody:
        content:
          application/json:
            schema:
              type: object
              properties:
                field1:
                  type: string
                  description: |
                    for UK this is Account number
                field2:
                  type: string
                  description: for UK this is sort-code
      security:
        - OIDC: []
      description: |-
        Update bank info.
        field1 is:
          UK - Account Number

        field2 is:
          UK - sort-code
      tags:
        - Account
  '/profile/{id}/location':
    parameters:
      - schema:
          type: string
        name: id
        in: path
        required: true
    post:
      summary: Update user location
      operationId: post-profile-id-location
      responses:
        '200':
          description: OK
        '400':
          description: Bad Request
        '403':
          description: Forbidden
      requestBody:
        content:
          application/json:
            schema:
              type: object
              properties:
                lat:
                  type: string
                lng:
                  type: string
      parameters: []
      description: Send location updates to this endpoint
      tags:
        - profile
        - location
  '/profile/{id}/profile-image':
    parameters:
      - schema:
          type: string
        name: id
        in: path
        required: true
    get:
      summary: Profile Image
      tags:
        - profile
        - image
      responses:
        '200':
          description: OK
          content:
            image/jpeg:
              schema: {}
        '404':
          description: Not Found
      operationId: get-profile-id-profile-image
      description: Get a jpeg of the current profile image
    post:
      summary: Upload profile Image
      operationId: post-profile-id-profile-image
      responses:
        '200':
          description: OK
      requestBody:
        content:
          multipart/form-data:
            schema: {}
        description: Form-data including a file for the image
      description: 'Upload new profile image, overwriting the current image'
      tags:
        - profile
        - image
  /profiles/search:
    get:
      summary: Find nearby profiles
      tags:
        - profile
        - search
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ProfileDetails'
      operationId: get-profiles-search
      requestBody:
        content:
          application/json:
            schema:
              type: object
              properties:
                lat:
                  type: number
                lng:
                  type: number
        description: Current user location info for search
      description: Get a list of profiles that are within 100m of the supplied location
components:
  schemas:
    AccountOverview:
      title: AccountOverviewModel
      type: object
      properties:
        name:
          type: string
        gratuuId:
          type: string
        payeInfoValid:
          type: boolean
        accountBalance:
          type: number
        historicalTotal:
          type: number
    PaymentInfo:
      title: PaymentInfoModel
      type: object
      description: Information about a payment intent that has been generated on the payment gateway
      properties:
        id:
          type: string
        clientSecret:
          type: string
        amount:
          type: number
        tipId:
          type: number
        paid:
          type: boolean
        gatewayReference:
          type: string
          description: Reference used when communicating with the payment gateway. eg Stripe Payment Reference
    CreateTipModel:
      title: CreateTipModel
      type: object
      properties:
        gratuuId:
          type: string
        amount:
          type: string
    TipInfo:
      title: TipInfoModel
      type: object
      properties:
        id:
          type: string
        amount:
          type: number
        paid:
          type: boolean
        fromId:
          type: string
        fromName:
          type: string
        toId:
          type: string
        toName:
          type: string
        tipDate:
          type: string
    PAYEInfo:
      title: PAYEInfoModel
      type: object
      properties:
        gratuuId:
          type: string
        firstName:
          type: string
        surname:
          type: string
        nationalInsurance:
          type: string
        addressLine1:
          type: string
        addressLine2:
          type: string
        town:
          type: string
        city:
          type: string
        county:
          type: string
        postcode:
          type: string
        bankSortCode:
          type: string
        bankAccountNumber:
          type: string
        gender:
          type: number
          description: 'Male = 1, Female = 2'
        dob:
          type: string
    ProfileDetails:
      title: ProfileDetails
      type: object
      properties:
        name:
          type: string
          description: |
            Display name of the profile, currently firstname+surname from account object
        gratuuId:
          type: string
        image:
          type: string
          description: URL for profile image
        isTronc:
          type: string
          description: Is this profile an organisation that is acting as a tronc master
        jobTitle:
          type: string
        companyName:
          type: string
      description: Full detail model for a profile
  securitySchemes:
    OIDC:
      type: openIdConnect
      openIdConnectUrl: 'https://auth.gratuu.com/.well-known/openid-configuration'
tags:
  - name: paye
  - name: Payment
  - name: tips
  - name: Account
  - name: profile
  - name: location
  - name: image
  - name: search
